const valores = [true, 5, false, "hola", "adios", 2];

if(valores[3].length > valores[4].length){
    console.log(`la cadena ${valores[3]} es mayor`);
} else {
    console.log(`La cadena ${valores[4]} es mayor`)
}

if(valores[0] && valores[2]){
    console.log('es verdaderos');
} else{
    console.log('es falso');
}

if(valores[0] || valores[2]){
    console.log('es verdaderos')
}

console.log(`La suma es: ${valores[1] + valores[5]}`)
console.log(`La resta es: ${valores[1] - valores[5]}`)
console.log(`La division es: ${valores[1] / valores[5]}`)
console.log(`La multiplicacion es: ${valores[1] * valores[5]}`)
console.log(`El modulo es: ${valores[1]% valores[5]}`)